PIC32 ILI9341 LCD driver
====================== 

PIC32 Microchip Harmony GFX Driver for ILI9341 based low cost LCD displays.

Requirements
------------
Microchip Harmony v1.07.01 for PIC32 devices


Usage
-----

8 bit interface supported via PMP driver

The diver can be added to the Harmony framework by placing the files into the following directory:
$MICROCHIP_HARMONY_DIR/framework/driver/gfx/controller/ili9341

To enable it in the MH Configurator the following framework files have to be modified as well:
...
TODO


Author
------
V.Govorovski


