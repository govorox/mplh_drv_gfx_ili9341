/*
 ******************************************************************************
  Company:
    Microchip Technology Incorporated

  File Name:
    drv_gfx_ili9341.c

  Summary:
    Driver library for device ILI9341 with TFT display connected

  Description:
    Driver library for device ILI9341 includes:
    - Writing to device registers and GRAM using PMP Driver
    - Initialization of the ILI9341
    - Supports rotation of the display
    - Supports displaying single pixel, n pixels, array of pixels

    PMP Bus width is 8 bit, GRAM address is 16 bit(8 - horizontal, 8 -
    vertical) and color format if 16 bit(5 - Red, 6 - Green, 5 - Blue)
 *******************************************************************************
*/
/*
******************************************************************************
  Software License Agreement
  Copyright ? 2008 Microchip Technology Inc.  All rights reserved.
  Microchip licenses to you the right to use, modify, copy and distribute
  Software only when embedded on a Microchip microcontroller or digital
  signal controller, which is integrated into your product or third party
  product (pursuant to the sublicense terms in the accompanying license
  agreement).

  You should refer to the license agreement accompanying this Software
  for additional information regarding your rights and obligations.

  SOFTWARE AND DOCUMENTATION ARE PROVIDED ?AS IS? WITHOUT WARRANTY OF ANY
  KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY
  OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
  PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR
  OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,
  BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT
  DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
  INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA,
  COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY
  CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF),
  OR OTHER SIMILAR COSTS.
*******************************************************************************
*/

#include "../drv_gfx_ili9341.h"


//*****************************************************************************

#define ILI9341_SOFTRESET         0x01
#define ILI9341_SLEEPIN           0x10
#define ILI9341_SLEEPOUT          0x11
#define ILI9341_NORMALDISP        0x13
#define ILI9341_INVERTOFF         0x20
#define ILI9341_INVERTON          0x21
#define ILI9341_GAMMASET          0x26
#define ILI9341_DISPLAYOFF        0x28
#define ILI9341_DISPLAYON         0x29
#define ILI9341_COLADDRSET        0x2A
#define ILI9341_PAGEADDRSET       0x2B
#define ILI9341_MEMORYWRITE       0x2C
#define ILI9341_MEMORYREAD        0x2E
#define ILI9341_PIXELFORMAT       0x3A
#define ILI9341_FRAMECONTROL      0xB1
#define ILI9341_DISPLAYFUNC       0xB6
#define ILI9341_ENTRYMODE         0xB7
#define ILI9341_POWERCONTROL1     0xC0
#define ILI9341_POWERCONTROL2     0xC1
#define ILI9341_VCOMCONTROL1      0xC5
#define ILI9341_VCOMCONTROL2      0xC7
#define ILI9341_MEMCONTROL        0x36
#define ILI9341_MADCTL            0x36

#define ILI9341_MADCTL_MY         0x80
#define ILI9341_MADCTL_MX         0x40
#define ILI9341_MADCTL_MV         0x20
#define ILI9341_MADCTL_ML         0x10
#define ILI9341_MADCTL_RGB        0x00
#define ILI9341_MADCTL_BGR        0x08
#define ILI9341_MADCTL_MH         0x04


// *****************************************************************************
/* ILI9341 Driver task states

  Summary
    Lists the different states that ILI9341 task routine can have.

  Description
    This enumeration lists the different states that OTM2201a task routine can have.

  Remarks:
    None.
*/

typedef enum
{
    /* Process queue */
    DRV_GFX_ILI9341_INITIALIZE_START,

    DRV_GFX_ILI9341_INITIALIZE_PWR_UP,

    DRV_GFX_ILI9341_INITIALIZE_SET_REGISTERS,

    /* GFX ILI9341 task initialization done */
    DRV_GFX_ILI9341_INITIALIZE_DONE,

} DRV_GFX_ILI9341_OBJECT_TASK;

// *****************************************************************************
/* GFX ILI9341 Driver Instance Object

  Summary:
    Defines the object required for the maintenance of the hardware instance.

  Description:
    This defines the object required for the maintenance of the hardware
    instance. This object exists once per hardware instance of the peripheral.

  Remarks:
    None.
*/

typedef struct _DRV_GFX_ILI9341_OBJ
{
    /* Flag to indicate in use  */
    bool                                        inUse;

    /* Save the index of the driver */
    SYS_MODULE_INDEX                            drvIndex;

    /* ILI9341 machine state */
    DRV_GFX_STATES                              state;

    /* Status of this driver instance */
    SYS_STATUS                                  status;

    /* Number of clients */
    uint32_t                                    nClients;

    /* Client of this driver */
    DRV_GFX_CLIENT_OBJ *                        pDrvILI9341ClientObj;

    /* State of the task */
    DRV_GFX_ILI9341_OBJECT_TASK    		task;

    DRV_GFX_INIT *                              initData;

    uint16_t      maxY;
    uint16_t      maxX;

} DRV_GFX_ILI9341_OBJ;

static DRV_GFX_ILI9341_OBJ        drvILI9341Obj;

static DRV_GFX_CLIENT_OBJ          drvILI9341Clients;

/*
 ******************************************************************************
 *                      Gloabal Variables
 ******************************************************************************
 */

DRV_GFX_INTERFACE otmInterface;

//global instance for the driver
uint8_t             _instance;
//PMP Data Width
//uint8_t             _dataWidth;
//pmp driver handle
DRV_HANDLE          pmphandle;
DRV_PMP_MODE_CONFIG config;

//This contains the commands to send to the graphics driver
DRV_GFX_ILI9341_COMMAND  ILI9341commandBuffer[1];

//pointer to where the command Buffer is currently
DRV_GFX_ILI9341_COMMAND* ILI9341queueIndex = &ILI9341commandBuffer[0];

//Flag to state driver is busy
volatile uint16_t   ILI9341driverBusy   = false;
//Flag to state task is busy
volatile uint8_t    ILI9341taskBusy     = false;
//Flag to state task is Ready
volatile uint8_t    ILI9341taskReady    = false;

//holds instances of GFX_CONFIG_DRIVER_COUNT number of drivers
uint8_t instances[GFX_CONFIG_DRIVER_COUNT] = {0};

uint8_t xCount = 0;
uint8_t yCount = 0;

#define GFX_PMP_DeviceWrite(data) PLIB_PMP_MasterSend(0, data); \
                                  while(PMMODEbits.BUSY == 1)

uint8_t GFX_PMP_DeviceRead();

// forward declarations
void _DelayMs(uint16_t msDelay);

//void _command(uint8_t cmd);
//void _data(uint8_t dat);

#define _command(cmd) DisplaySetCommand(); GFX_PMP_DeviceWrite(cmd);
#define _data(dat) DisplaySetData(); GFX_PMP_DeviceWrite(dat);

void _init(void);
void _window(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1);
void _rotate(uint16_t rotation);

// *****************************************************************************
/*
  Function: uint16_t DRV_GFX_ILI9341_Initialize(uint8_t instance)

  Summary:
    resets LCD, initializes PMP

  Description:
    none

  Input:
        instance - driver instance
  Output:
    1 - call not successful (PMP driver busy)
    0 - call successful
*/
SYS_MODULE_OBJ DRV_GFX_ILI9341_Initialize(const SYS_MODULE_INDEX   index,
                                           const SYS_MODULE_INIT    * const init)
{

    static uint8_t  state           = 0;
    static uint16_t horizontalSize;
    static uint16_t verticalSize;
    static uint16_t dummy           = 1;
    static uint16_t entryMode       = 0;
    static uint16_t scanMode        = 0x001C;

    /* Validate the driver index */
    if ( index >= GFX_CONFIG_NUMBER_OF_MODULES )
    {
        return SYS_MODULE_OBJ_INVALID;
    }

    DRV_GFX_ILI9341_OBJ *dObj = &drvILI9341Obj;

    /* Object is valid, set it in use */
    dObj->inUse = true;
    dObj->state = SYS_STATUS_BUSY;
    dObj->initData = (DRV_GFX_INIT *) init;

    /* Save the index of the driver. Important to know this
    as we are using reference based accessing */
    dObj->drvIndex = index;

    dObj->task = DRV_GFX_ILI9341_INITIALIZE_START; //SET_REGISTERS;

    //while (state <= 32)
    {
        switch (state) {
            case 0:
                
                dObj->task = DRV_GFX_ILI9341_INITIALIZE_PWR_UP;

                DisplayResetEnable(); // hold in reset by default
                DisplayResetConfig(); // enable RESET line
                DisplayCmdDataConfig();
                DisplayConfig(); // enable chip select line

               _DelayMs(10);
                               
                // release from reset
                DisplayResetDisable();

                DisplayEnable();
                
                /*
                if ((drvILI9341Obj.initData->orientation == DEGREE_90) ||
                    (drvILI9341Obj.initData->orientation == DEGREE_270))
                {
                    verticalSize =
                                drvILI9341Obj.initData->horizontalResolution;
                    horizontalSize =
                                drvILI9341Obj.initData->verticalResolution;
                }
                else
                 */
                {
                    horizontalSize =
                                drvILI9341Obj.initData->horizontalResolution;
                    verticalSize =
                                drvILI9341Obj.initData->verticalResolution;
                }
                
                drvILI9341Obj.maxY = verticalSize - 1;
                drvILI9341Obj.maxX = horizontalSize - 1;
/*
                if ((drvILI9341Obj.initData->orientation == DEGREE_90) ||
                        (drvILI9341Obj.initData->orientation == DEGREE_270)) {
                    horizontalSize =
                            drvILI9341Obj.initData->verticalResolution;
                    verticalSize =
                            drvILI9341Obj.initData->horizontalResolution;
                    drvILI9341Obj.maxX = horizontalSize - 1;
                    drvILI9341Obj.maxY = verticalSize - 1;
                } else {
                    horizontalSize =
                            drvILI9341Obj.initData->horizontalResolution;
                    verticalSize =
                            drvILI9341Obj.initData->verticalResolution;
                    drvILI9341Obj.maxX = verticalSize - 1;
                    drvILI9341Obj.maxY = horizontalSize - 1;
                }

                switch (drvILI9341Obj.initData->orientation) {
                    case DEGREE_90:
                        entryMode = 0x1008;
                        scanMode = 0x011C;
                        break;

                    case DEGREE_180:
                        entryMode = 0x1000;
                        scanMode = 0x011C;
                        break;

                    case DEGREE_270:
                        entryMode = 0x1008;
                        scanMode = 0x031C;
                        break;

                    case DEGREE_0:
                    default:
                        entryMode = 0x1030;
                        scanMode = 0x001C;
                        break;
                }

                break;
*/
            case 1:
                //while (!dummy)
                //    while (DRV_GFX_ILI9341_RegGet(REG_POWER_CTRL_2, &dummy));

                state = 2;
                break;

            case 3:
                break;

            default:
                break;
        }

        /*
        //ILI9341 Registers to be initialized
        uint16_t registers[33][2] = {
            { REG_POWER_CTRL_2, 0x0018 },
            { REG_POWER_CTRL_3, 0x0000 },
            { REG_POWER_CTRL_4, 0x0063 },
            { REG_POWER_CTRL_5, 0x556A },
            { REG_POWER_CTRL_1, 0x0800 },
            { REG_POWER_CTRL_2, 0x0118 },
            { REG_POWER_CTRL_2, 0x0318 },
            { REG_POWER_CTRL_2, 0x0718 },
            { REG_POWER_CTRL_2, 0x0F18 },
            { REG_POWER_CTRL_2, 0x0F38 },
            { REG_DISP_CTRL, 0x001A },
            { REG_DRV_OUT_CTRL, scanMode },
            { REG_ENTRY_MODE, entryMode },
            { REG_DISP_CTRL, 0x0000 },
            { REG_BLANKING_CTRL, 0x0808 },
            { REG_VCI_PERIOD, 0x0020 },
            { REG_HORZ_WND_ADDR_1, 0x00AF },
            { REG_HORZ_WND_ADDR_2, 0x0000 },
            { REG_VERT_WND_ADDR_1, 0x00DB },
            { REG_VERT_WND_ADDR_2, 0x0000 },
            { REG_GAMMA_CTRL_1, 0x0001 },
            { REG_GAMMA_CTRL_2, 0x0208 },
            { REG_GAMMA_CTRL_3, 0x0805 },
            { REG_GAMMA_CTRL_4, 0x0404 },
            { REG_GAMMA_CTRL_5, 0x0C0C },
            { REG_GAMMA_CTRL_6, 0x000C },
            { REG_GAMMA_CTRL_7, 0x0100 },
            { REG_GAMMA_CTRL_8, 0x0400 },
            { REG_GAMMA_CTRL_9, 0x1108 },
            { REG_GAMMA_CTRL_10, 0x050C },
            { REG_OSC_CTRL, 0x1F01 },
            { REG_DISP_CTRL, 0x0012 },
            { REG_DISP_CTRL, 0x0017 }
        };
        */

        //while (DRV_GFX_ILI9341_RegSet(*registers[state],
        //        registers[state][1]));
        
        dObj->task = DRV_GFX_ILI9341_INITIALIZE_SET_REGISTERS;
        
        _init();
        
        _rotate(drvILI9341Obj.initData->orientation);
        
        DRV_GFX_ILI9341_BarFill(0, 0, drvILI9341Obj.maxX, drvILI9341Obj.maxY);

        state++;
    }

    //drvILI9341Obj.initData->horizontalResolution = horizontalSize;
    //drvILI9341Obj.initData->verticalResolution = verticalSize;

    dObj->nClients = 0;
    dObj->status = SYS_STATUS_READY;

    /* Return the driver handle */
    return (SYS_MODULE_OBJ)dObj;
}

// *****************************************************************************
/*
  Function: DRV_HANDLE DRV_GFX_ILI9341_Open( const SYS_MODULE_INDEX index,
                             const DRV_IO_INTENT intent )

  Summary:
    opens an instance of the graphics controller

  Description:
    none

  Input:
    instance of the driver

  Output:
    1 - driver not initialied
    2 - instance doesn't exist
    3 - instance already open
    instance to driver when successful
*/
DRV_HANDLE DRV_GFX_ILI9341_Open( const SYS_MODULE_INDEX index,
                             const DRV_IO_INTENT intent )
{
   DRV_GFX_CLIENT_OBJ * client = (DRV_GFX_CLIENT_OBJ *)DRV_HANDLE_INVALID;

    /* Check if the specified driver index is in valid range */
    if(index >= GFX_CONFIG_NUMBER_OF_MODULES)
    {
        //SYS_DEBUG(0, "GFX_SSD1926 Driver: Bad Driver Index");
    }
    /* Check if instance object is ready*/
    else if(drvILI9341Obj.status != SYS_STATUS_READY)
    {
        /* The SSD1926 module should be ready */
//        SYS_DEBUG(0, "GFX_SSD1926 Driver: Was the driver initialized?");
    }
    else if(intent != DRV_IO_INTENT_EXCLUSIVE)
    {
        /* The driver only supports this mode */
//        SYS_DEBUG(0, "GFX_SSD1926 Driver: IO intent mode not supported");
    }
    else if(drvILI9341Obj.nClients > 0)
    {
        /* Driver supports exclusive open only */
//        SYS_DEBUG(0, "GFX_SSD1926 already opened once. Cannot open again");
    }
    else
    {
        client = &drvILI9341Clients;

        client->inUse = true;
        client->drvObj = &drvILI9341Obj;

        /* Increment the client number for the specific driver instance*/
        drvILI9341Obj.nClients++;
    }

    /* Return invalid handle */
    return ((DRV_HANDLE)client);
}

// *****************************************************************************
/* Function:
    void DRV_GFX_ILI9341_Close( DRV_HANDLE handle )

  Summary:
    closes an instance of the graphics controller

  Description:
    This is closes the instance of the driver specified by handle.
*/
void DRV_GFX_ILI9341_Close( DRV_HANDLE handle )
{
    /* Start of local variable */
    DRV_GFX_CLIENT_OBJ * client = (DRV_GFX_CLIENT_OBJ *)NULL;
    DRV_GFX_ILI9341_OBJ * drvObj = ( DRV_GFX_ILI9341_OBJ *)NULL;
    /* End of local variable */

    /* Check if the handle is valid */
    if(handle == DRV_HANDLE_INVALID)
    {
//        SYS_DEBUG(0, "Bad Client Handle");
    }
    else
    {
        client = (DRV_GFX_CLIENT_OBJ *)handle;

        if(client->inUse)
        {
            client->inUse = false;
            drvObj = ( DRV_GFX_ILI9341_OBJ *)client->drvObj;

            /* Remove this client from the driver client table */
            drvObj->nClients--;
        }
        else
        {
//            SYS_DEBUG(0, "Client Handle no inuse");
        }
    }
    return;
}

// *****************************************************************************
/*
  Function:
     uint16_t DRV_GFX_ILI9341_MaxXGet(void)

  Summary:
     Returns x extent of the display.

  Description:

  Precondition:

  Parameters:

  Returns:

  Example:
    <code>
    <code>

  Remarks:
*/
uint16_t DRV_GFX_ILI9341_MaxXGet(void)
{
    return drvILI9341Obj.maxX;
}

// *****************************************************************************
/*
  Function:
     uint16_t DRV_GFX_ILI9341_MaxYGet(void)

  Summary:
     Returns y extent of the display.

  Description:

  Precondition:

  Parameters:

  Returns:

  Example:
    <code>
    <code>

  Remarks:
*/
uint16_t DRV_GFX_ILI9341_MaxYGet(void)
{
    return drvILI9341Obj.maxY;
}

/*********************************************************************
  Function:
     DRV_GFX_INTEFACE DRV_GFX_ILI9341_InterfaceGet( DRV_HANDLE handle )

  Summary:
    Returns the API of the graphics controller

  Description:
    none

  Return:

  *********************************************************************/
void DRV_GFX_ILI9341_InterfaceSet(DRV_HANDLE handle, DRV_GFX_INTERFACE *interface )
{
    interface->BarFill = DRV_GFX_ILI9341_BarFill;
    interface->PixelArrayPut = DRV_GFX_ILI9341_PixelArrayPut;
    interface->PixelArrayGet = DRV_GFX_ILI9341_PixelArrayGet;
    interface->PixelPut = DRV_GFX_ILI9341_PixelPut;
    interface->ColorSet = DRV_GFX_ILI9341_ColorSet;
    interface->MaxXGet = DRV_GFX_ILI9341_MaxXGet;
    interface->MaxYGet = DRV_GFX_ILI9341_MaxYGet;
}

/*
*******************************************************************************
  Function: uint16_t DRV_GFX_ILI9341_AddressSet(uint32_t address)

  Summary:
    Sets the start GRAM address where pixel data to be written

  Description:
    Address consists of: 
        Lower 8 bit at Register REG_RAM_ADDR_LOW
        Higher 8 bit at Register REG_RAM_ADDR_HIGH

  Input:
    address - address

  Output:
    DRV_ILI9341_ERROR_PMP_WRITE - returns error during PMP Write
    DRV_ILI9341_ERROR_NO_ERROR - returns success without any error
*******************************************************************************
*/

/*
uint16_t DRV_GFX_ILI9341_AddressSet(uint32_t address)
{
    static uint32_t data = 0x00000000;
    
    DisplaySetCommand(); // set RS line to low for command
    GFX_PMP_DeviceWrite(0x00);
    GFX_PMP_DeviceWrite(REG_RAM_ADDR_LOW);
    DisplaySetData(); // set RS line to low for command

    data  = address & DRV_ILI9341_ADDR_LOW_MASK;
    GFX_PMP_DeviceWrite(data>>8);
    GFX_PMP_DeviceWrite(data);
    
    DisplaySetCommand(); // set RS line to low for command
    GFX_PMP_DeviceWrite(0x00);
    GFX_PMP_DeviceWrite(REG_RAM_ADDR_HIGH);
    DisplaySetData(); // set RS line to low for command

    data = (address & DRV_ILI9341_ADDR_HIGH_MASK)
            >> DRV_ILI9341_ADDR_HIGH_SHIFT;

    GFX_PMP_DeviceWrite(data>>8);
    GFX_PMP_DeviceWrite(data);
    
    return(DRV_ILI9341_ERROR_NO_ERROR);
}
*/

/*
 *****************************************************************************
  Function: uint8_t DRV_GFX_ILI9341_RegSet(uint16_t index,
                                            uint16_t value,
                                            uint32_t repeatCount)

  Summary:
    updates graphics controller register value (byte access)

  Description:
    This call can set "value" of the register accessed by its "index" and can
 repeat the same by number of times mentioned in "repeatCount"

  Input:
    index       - register number
    value       - value to write to register
    repeatCount - repeatCount number of times value is to be written to the
                    register.

  Output:
    DRV_ILI9341_ERROR_PMP_WRITE - returns error during PMP Write
    DRV_ILI9341_ERROR_NO_ERROR - returns success without any error
 
*******************************************************************************
*/
uint16_t DRV_GFX_ILI9341_RegSet(uint16_t index,
                                uint16_t value)
{

    DisplaySetCommand(); // set RS line to low for command
    //GFX_PMP_DeviceWrite(0x00);
    GFX_PMP_DeviceWrite(index);
    DisplaySetData(); // set RS line to low for command

    //PLIB_PMP_MasterSend(0, value>>8);
    //while(PMMODEbits.BUSY == 1);

    //PLIB_PMP_MasterSend(0, value);
    //while(PMMODEbits.BUSY == 1);

    return(DRV_ILI9341_ERROR_NO_ERROR);
}

/*
*******************************************************************************
  Function: uint8_t DRV_GFX_ILI9341_RegGet(
                                            uint16_t index,
                                            uint8_t *data
                                            )

  Summary:
    returns graphics controller register value (byte access)

  Description:
    returns graphics controller register value (byte access)

  Input:
    index - register number
    *data - array to store register data

  Output:
    DRV_ILI9341_ERROR_PMP_WRITE - returns error during PMP Write
    DRV_ILI9341_ERROR_PMP_READ  - returns error during PMP Read
    DRV_ILI9341_ERROR_NO_ERROR  - returns success without any error
*******************************************************************************
*/
uint8_t  DRV_GFX_ILI9341_RegGet(uint16_t  index, uint16_t *data)
{

    static uint16_t myReadBuffer = 0x0000;

    DisplaySetCommand(); // set RS line to low for command
    //GFX_PMP_DeviceWrite(0x00);
    GFX_PMP_DeviceWrite(index);
    DisplaySetData(); // set RS line to low for command

    myReadBuffer = PMDIN;
    while (PMMODEbits.BUSY);
    PMCONbits.PMPEN = 0; // disable PMP
    myReadBuffer = PMDIN;
    while (PMMODEbits.BUSY);
    PMCONbits.PMPEN = 1; // enable  PMP

    *data = PMDIN;
    while (PMMODEbits.BUSY);
    PMCONbits.PMPEN = 0; // disable PMP
    myReadBuffer = PMDIN;
    while (PMMODEbits.BUSY);
    PMCONbits.PMPEN = 1; // enable  PMP

    *data  = myReadBuffer;
    return(DRV_ILI9341_ERROR_NO_ERROR);
    
}

/*
*******************************************************************************

  Function: uint8_t DRV_GFX_ILI9341_BrightnessSet(
    uint8_t instance,
    uint16_t level
    )

  Summary:
    Sets the brightness of the display backlight.

  Description:
    Sets the brightness of the display backlight.

  Input:
        level - Brightness level. Valid values are 0 to 100.
		0   = brightness level is zero or display is turned off.
                100 = brightness level is maximum.

  Output:
    none
 
********************************************************************************
*/
void DRV_GFX_ILI9341_BrightnessSet(uint8_t instance, uint16_t  level)
{

    // If the brightness can be controlled (for example through PWM)
    // add code that will control the PWM here.

    if (level > 0)
    {
        DisplayBacklightOn();
    }
    else if (level == 0)
    {
        DisplayBacklightOff();
    }

}

/********************************************************************************

  Function: void DRV_GFX_ILI9341_ColorSet(GFX_COLOR color)

  Summary:
    Sets the color for the driver instance

  Description:
    Sets the color for the driver instance

  Input:
    color - 16 bit 565 format color value

  Output: none

*******************************************************************************
*/

void DRV_GFX_ILI9341_ColorSet(GFX_COLOR color)
{
     drvILI9341Obj.initData->color = color;
}

/*
*******************************************************************************

  Function: void DRV_GFX_ILI9341_PixelPut(uint16_t x, uint16_t y)

  Summary:
    outputs one pixel into the frame buffer at the x,y coordinate given

  Description:
    none

  Input:
        x           - pixel coordinate on x axis
        y           - pixel coordinate on y axis
 
  Output:

        DRV_ILI9341_ERROR_QUEUE_FULL   - ILI9341 command queue is full
        DRV_ILI9341_ERROR_NO_ERROR     - Success without any error

*******************************************************************************
*/
void DRV_GFX_ILI9341_PixelPut(uint16_t x, uint16_t y)
{

     //ILI9341queueIndex->address     = _PixelStartAddress(x, y,
     //                               drvILI9341Obj.initData->orientation);

     //while(DRV_GFX_ILI9341_AddressSet(ILI9341queueIndex->address));

    _window(x, y, x+1, y+1);
    _data(drvILI9341Obj.initData->color >> 8);
    _data(drvILI9341Obj.initData->color);
     
     //DisplaySetData(); // set RS line to low for command

     //GFX_PMP_DeviceWrite(drvILI9341Obj.initData->color >> 8);
     //GFX_PMP_DeviceWrite(drvILI9341Obj.initData->color);

}

/*
*******************************************************************************
  Function: void DRV_GFX_ILI9341_BarFill(
                                                uint16_t left,
                                                uint16_t top,
                                                uint16_t right,
                                                uint16_t bottom
                                                )

  Summary:
    outputs count number of pixels into the frame buffer from the given x,y
    coordinate.

  Description:
    outputs count number of pixels into the frame buffer from the given x,y
    coordinate.

  Input:
 
        left           - pixel coordinate on x axis
        top           - pixel coordinate on y axis
        right       - pixel coordinate on x axis
        bottom   - pixel coordinate on y axis

  Output:

    DRV_ILI9341_ERROR_QUEUE_FULL   - ILI9341 command queue is full
    DRV_ILI9341_ERROR_NO_ERROR     - Success without any error

*******************************************************************************
*/
void  DRV_GFX_ILI9341_BarFill(uint16_t left,
                              uint16_t top,
                              uint16_t right,
                              uint16_t bottom)
{
    uint16_t count = right - left + 1;
    uint16_t lineCount = bottom - top + 1;

    uint16_t _y = 0;
    uint16_t tempCount = count;
    
    //SYS_PRINT("\r\nDRV_GFX BarFill l=%d,t=%d,r=%d,b=%d",
    //          left, top, right, bottom);
    
    //static uint32_t address                 = 0;

    //ILI9341queueIndex->address     = _PixelStartAddress(left, top,
    //                                drvILI9341Obj.initData->orientation);

    /*
    while (lineCount) {
        address = _PixelAddressUpdate((ILI9341queueIndex->address & 0x00FF),
                ((ILI9341queueIndex->address & 0xFF00) >> 8),
                _y,
                drvILI9341Obj.initData->orientation);

        while (DRV_GFX_ILI9341_AddressSet(address));

        DisplaySetCommand(); // set RS line to low for command
        GFX_PMP_DeviceWrite(0x00);
        GFX_PMP_DeviceWrite(REG_GRAM_DATA);
        DisplaySetData(); // set RS line to low for command

        while (count) {
            GFX_PMP_DeviceWrite(drvILI9341Obj.initData->color >> 8);
            GFX_PMP_DeviceWrite(drvILI9341Obj.initData->color);
            count--;
        }
        
        count = tempCount;
        lineCount--;
        _y++;
    }
    */
    
    _window(left, top, right, bottom);
    
    while(lineCount) {
        while (count) {
            //DisplaySetData(); // set RS for data
            //GFX_PMP_DeviceWrite(drvILI9341Obj.initData->color >> 8);
            //GFX_PMP_DeviceWrite(drvILI9341Obj.initData->color);
            //
            
            _data(drvILI9341Obj.initData->color >> 8);
            _data(drvILI9341Obj.initData->color);
            
            count--;
        }
        top++;
        count = tempCount;
        lineCount--;
        //_y++;
	}
}

/*
*******************************************************************************
  Function: void  DRV_GFX_ILI9341_PixelArrayPut( 
                                                      uint16_t *color,
                                                      uint16_t x,
                                                      uint16_t y,
                                                      uint16_t count,
                                                      uint16_t lineCount
                                                    )

  Summary:
    outputs an array of pixels of length count starting at *color

  Description:
    outputs an array of pixels of length count starting at *color

  Input:
        color       - pointer to array of color of pixels
        x           - pixel coordinate on x axis.
        y           - pixel coordinate on y axis.
        count       - count number of pixels
        lineCount   - lineCount number of display lines
 
  Output:
         handle - handle to the number of pixels remaining
         DRV_ILI9341_ERROR_QUEUE_FULL   - ILI9341 command queue is full

*******************************************************************************
*/
void  DRV_GFX_ILI9341_PixelArrayPut( 
                                    uint16_t *color,
                                    uint16_t x,
                                    uint16_t y,
                                    uint16_t count,
                                    uint16_t lineCount)
{
    uint16_t _y = 0;
    uint16_t tempCount = count;
    static uint32_t address = 0;


    //ILI9341queueIndex->address     = _PixelStartAddress(x, y,
    //                                drvILI9341Obj.initData->orientation);
    
    _window(x, y, x+count, y+lineCount);
    

    while (lineCount) {
        //address = _PixelAddressUpdate((ILI9341queueIndex->address & 0x00FF),
        //        ((ILI9341queueIndex->address & 0xFF00) >> 8),
        //        _y,
        //        drvILI9341Obj.initData->orientation);

        //if (DRV_GFX_ILI9341_AddressSet(address)) {
        //    return;
        //}
        
        while(count)
        {
            _data(*color >> 8);
            _data(*color);
    
            //DisplaySetData(); // set RS for data
            //GFX_PMP_DeviceWrite(*color>>8);
            //GFX_PMP_DeviceWrite(*color);
            
            color++;
            count--;
        }

        count = tempCount;
        lineCount--;
        //_y++;
    }

    ILI9341queueIndex->count = 0;
    return;

}

/*
*******************************************************************************

  Function: uint16_t  DRV_GFX_ILI9341_PixelArrayGet( 
 *                                                  uint16_t *color,
                                                    uint16_t x,
                                                    uint16_t y,
                                                    uint16_t count)

  Summary:
    gets an array of pixels of length count into an array starting at *color

  Description:
    gets an array of pixels of length count into an array starting at *color

  Input:
            color   - Pointer to array where color data is to be loaded
            x       - pixel coordinate on x axis
            y       - pixel coordinate on y axis
            count   - count number of pixels
  Output:
         DRV_ILI9341_ERROR_QUEUE_FULL  - ILI9341 command queue is full
         DRV_ILI9341_ERROR_NO_ERROR    - Success without any error

*******************************************************************************
*/

uint16_t*  DRV_GFX_ILI9341_PixelArrayGet(
                                        uint16_t *color,
                                        uint16_t x,
                                        uint16_t y,
                                        uint16_t count)
{
    _window(x, y, x+count, y+1);
    _command(ILI9341_MEMORYREAD);
    // Read data from memory
    DisplaySetData();
    while(count)
    {
        uint16_t col = GFX_PMP_DeviceRead() << 8;
        col |= GFX_PMP_DeviceRead();
        *color = col;
        color++;
        count--;
    }
    
    return(DRV_ILI9341_ERROR_NO_ERROR);
}

/*
*******************************************************************************

  Function: uint16_t  DRV_GFX_ILI9341_Busy(uint8_t instance)

  Summary:
    Returns non-zero value if LCD controller is busy
          (previous drawing operation is not completed).

  Description:
    Returns non-zero value if LCD controller is busy
          (previous drawing operation is not completed).

  Input:
          instance - driver instance
 
  Output:
         DRV_ILI9341_ERROR_DEVICE_BUSY - Device is busy
         DRV_ILI9341_ERROR_NO_ERROR    - Success, driver is not busy
 
*******************************************************************************
*/

uint16_t  DRV_GFX_ILI9341_Busy(uint8_t instance)
{
    
   static uint16_t dummy = 0x01;

   //if(DRV_GFX_ILI9341_RegGet(REG_POWER_CTRL_2,(uint16_t*)&dummy))
   //     return(1);

   if(dummy == 1)
   {
      return(DRV_ILI9341_ERROR_NO_ERROR);
   }
   else
   {
      return(DRV_ILI9341_ERROR_DEVICE_BUSY);
   }
   
}
/*
uint32_t _PixelStartAddress(uint16_t x, uint16_t y, uint16_t orientation)
{
    uint32_t pixelStartAddress = 0;
    
    switch(orientation)
    {
        case DEGREE_90:

            pixelStartAddress = (((drvILI9341Obj.initData->horizontalResolution
                                 - x - 1) << 8) | y);

         break;

         case DEGREE_180:
            pixelStartAddress = (drvILI9341Obj.initData->horizontalResolution 
                                 - x - 1) | 
                                 ((drvILI9341Obj.initData->verticalResolution 
                                 - y - 1) << 8);
         break;

         case DEGREE_270:

            pixelStartAddress = ((drvILI9341Obj.initData->horizontalResolution 
                                 - x - 1) << 8) |
                                 (drvILI9341Obj.initData->verticalResolution 
                                 - y - 1);

         break;

         case DEGREE_0:
         default:

            pixelStartAddress = x | (y << 8);

         break;
    }
    
    return pixelStartAddress;
}

uint32_t _PixelAddressUpdate(uint16_t x, uint16_t y,
                            uint16_t lineCount, uint16_t orientation)
{
    uint32_t pixelAddressUpdate = 0;
    
    switch(orientation)
    {
        case DEGREE_90:

            pixelAddressUpdate = (x + lineCount) | (y << 8);

            break;

        case DEGREE_180:

            pixelAddressUpdate = x | ((y - lineCount) << 8);

            break;

        case DEGREE_270:

            pixelAddressUpdate = (x - lineCount) | (y << 8);

            break;

        case DEGREE_0:
        default:

            pixelAddressUpdate = x | ((y + lineCount) << 8);

            break;
    }
    
    return pixelAddressUpdate;
}

void _command(uint8_t cmd) {
    DisplaySetCommand();
    GFX_PMP_DeviceWrite(cmd);
}

void _data(uint8_t dat) {
    DisplaySetData();
    GFX_PMP_DeviceWrite(dat);
}
*/

void _DelayMs(uint16_t msDelay) {
    //SYS_TMR_HANDLE tmr = SYS_TMR_DelayMS ( msec );
    //while ( SYS_TMR_DelayStatusGet (tmr ) == false )
    //{
    //    SYS_Tasks ();
    //}
    ///while(msec-- > 0) {
        //_DelayMs(1);
        //SYS_Tasks();
    //}
    if (msDelay > 50) msDelay = 50;
    
    if(msDelay)
    {
        uint32_t sysClk = SYS_CLK_FREQ;
        uint32_t t0;
        t0 = _CP0_GET_COUNT();
        while (_CP0_GET_COUNT() - t0 < (sysClk/2000)*msDelay);
    }
}

void _window(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1) {
    
    if (x0 >= 240) x0 = 239;
    if (x1 >= 240) x1 = 239;
    if (y0 >= 320) y0 = 319;
    if (y1 >= 320) y1 = 319;
    
    
	_command(ILI9341_COLADDRSET); // Column addr set
    _data(x0 >> 8);
    _data(x0 & 0xFF);
    _data(x1 >> 8);
    _data(x1 & 0xFF);

	_command(ILI9341_PAGEADDRSET); // Row addr set
	_data(y0 >> 8);
	_data(y0 & 0xFF);     // YSTART
	_data(y1 >> 8);
	_data(y1 & 0xFF);     // YEND

	_command(ILI9341_MEMORYWRITE); // write to RAM
}

void _rotate(uint16_t rotation) {
    _command(ILI9341_MADCTL);
    switch (rotation) {
		case 0:
			_data(ILI9341_MADCTL_MX | ILI9341_MADCTL_BGR);
			break;
		case 90:
		    _data(ILI9341_MADCTL_MV | ILI9341_MADCTL_BGR);
			break;
		case 180:
			_data(ILI9341_MADCTL_MY | ILI9341_MADCTL_BGR);
			break;
		case 270:
			_data(ILI9341_MADCTL_MV | ILI9341_MADCTL_MY | ILI9341_MADCTL_MX | ILI9341_MADCTL_BGR);
			break;
    }
}

void _init(void) {
    _command(ILI9341_SOFTRESET);
    _DelayMs(10);
    _command(ILI9341_DISPLAYOFF);
    _command(ILI9341_POWERCONTROL1); _data(0x23);
    _command(ILI9341_POWERCONTROL2); _data(0x10);
    _command(ILI9341_VCOMCONTROL1); _data(0x2B); _data(0x2B);
    _command(ILI9341_VCOMCONTROL2); _data(0xC0);
    _command(ILI9341_MEMCONTROL); _data(ILI9341_MADCTL_MY | ILI9341_MADCTL_BGR);
    _command(ILI9341_PIXELFORMAT); _data(0x55);
    _command(ILI9341_FRAMECONTROL); _data(0x00); _data(0x1B);
    _command(ILI9341_ENTRYMODE); _data(0x07);
    
    
    _command(0xF2); // 3Gamma Function Disable
    _data(0x00);

    _command(0x26); // Gamma curve selected
    _data(0x01);

    _command(  0xE0); // Set Gamma
    _data(0x0F);
    _data(0x31);
    _data(0x2B);
    _data(0x0C);
    _data(0x0E);
    _data(0x08);
    _data(0x4E);
    _data(0xF1);
    _data(0x37);
    _data(0x07);
    _data(0x10);
    _data(0x03);
    _data(0x0E);
    _data(0x09);
    _data(0x00);

    _command(  0xE1); // Set Gamma
    _data(0x00);
    _data(0x0E);
    _data(0x14);
    _data(0x03);
    _data(0x11);
    _data(0x07);
    _data(0x31);
    _data(0xC1);
    _data(0x48);
    _data(0x08);
    _data(0x0F);
    _data(0x0C);
    _data(0x31);
    _data(0x36);
    _data(0x0F);

    _command(ILI9341_SLEEPOUT);
    _DelayMs(120);
    
    _command(ILI9341_DISPLAYON);
    //_DelayMs(500);
}

uint8_t GFX_PMP_DeviceRead()
{
    uint8_t v = PLIB_PMP_MasterReceive(0);
    while(PMMODEbits.BUSY == 1);
    return v;
}